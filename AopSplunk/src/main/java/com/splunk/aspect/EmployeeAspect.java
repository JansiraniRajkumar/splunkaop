package com.splunk.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import com.splunk.Receiver;
import com.splunk.Service;
import com.splunk.config.SplunkConfig;

@Aspect
@Component
public class EmployeeAspect {
	
	
	Service service = SplunkConfig.getSplunkService();
	Receiver rec = service.getReceiver();
	
	
	
	@Pointcut("execution(* com.rest.dao.EmployeeDao.*(..))")
	public void employeeRepository() {
		
	}
	

	@Pointcut("execution(* com.rest.service.EmployeeService.*(..))")
	public void employeeService() {
		
	}
	

	@Pointcut("execution(* com.rest.controller.EmployeeController.*(..))")
	public void employeeController() {
		
	}
	
	
	
	@Before("employeeService() && employeeController()")
	public void beforeAdvice(JoinPoint joinPoint) {
		rec.log("main","Before Advice Method in Employee Application  "+joinPoint.getSignature().getName()
				+ "  Method");
	

	}

	@After("employeeService() && employeeController()")
	public void afterAdviceMethod(JoinPoint joinPoint) {
		rec.log("main","After Advice Method in Employee Application  "+joinPoint.getSignature().getName()
				+ "  Method");
	
	}




}
