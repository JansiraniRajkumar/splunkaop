package com.splunk.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import com.splunk.Receiver;
import com.splunk.Service;
import com.splunk.config.SplunkConfig;

@Aspect
@Component
public class GatewayAspect {

	Service service = SplunkConfig.getSplunkService();
	Receiver rec = service.getReceiver();

	@Pointcut("execution(* com.APIGateway.controller.*(..))")
	public void gatewayController() {

	}

	@Before("gatewayController()")
	public void beforeAdvice(JoinPoint joinPoint) {
		rec.log("main", "Before Advice Method in Api Gateway Application FallBack controller "
				+ joinPoint.getSignature().getName() + "  Method");

	}

	@After("gatewayController()")
	public void afterAdviceMethod(JoinPoint joinPoint) {
		rec.log("main", "After Advice Method in Api Gateway Application FallBack controller  "
				+ joinPoint.getSignature().getName() + "  Method");

	}

}
