package com.splunk.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.splunk.Receiver;
import com.splunk.Service;
import com.splunk.config.SplunkConfig;

@Aspect
@Component
public class OrganizationAspect {
	

	Service service = SplunkConfig.getSplunkService();
	Receiver rec = service.getReceiver();
	
	@Pointcut("execution(* com.Organization_Service.service.OrganizationService.*(..))")
	public void organizationService() {
		
	}
	

	@Pointcut("execution(* com.Organization_Service.controller.OrganizationController.*(..))")
	public void organizationController() {
		
	}
	

	
	@Before("organizationService() && organizationController()")
	public void beforeAdvice(JoinPoint joinPoint) {
		rec.log("main","Before Advice Method in Organization Application  "+joinPoint.getSignature().getName()
				+ "  Method");
	

	}

	@After("organizationService() && organizationController()")
	public void afterAdviceMethod(JoinPoint joinPoint) {
		rec.log("main","After Advice Method in Organization Application  "+joinPoint.getSignature().getName()
				+ "  Method");
	
	}

	


}
