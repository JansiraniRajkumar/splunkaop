package com.splunk.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.splunk.Receiver;
import com.splunk.Service;
import com.splunk.config.SplunkConfig;

@Aspect
@Component
public class ClientAspect {
	
	
	
	Service service = SplunkConfig.getSplunkService();
	Receiver rec = service.getReceiver();
	
	@Pointcut("execution(* com.DeptEmpUI.controller.DeptController.*(..))")
	public void deptEmpController() {
		
	}
	

	
	@Before("deptEmpController()")
	public void beforeAdvice(JoinPoint joinPoint) {
		rec.log("main","Before Advice Method in DeptEmpUI Application department controller "+joinPoint.getSignature().getName()
				+ "  Method");
	

	}

	@After("deptEmpController()")
	public void afterAdviceMethod(JoinPoint joinPoint) {
		rec.log("main","After Advice Method in DeptEmpUI Application department controller  "+joinPoint.getSignature().getName()
				+ "  Method");
	
	}

	

	
	@Pointcut("execution(* com.DeptEmpUI.controller.EmployeeController.*(..))")
	public void deptEmployeeController() {
		
	}
	

	
	@Before("deptEmployeeController()")
	public void beforeAdviceEmployee(JoinPoint joinPoint) {
		rec.log("main","Before Advice Method in DeptEmpUI Application Employee controller "+joinPoint.getSignature().getName()
				+ "  Method");
	

	}

	@After("deptEmployeeController()")
	public void afterAdviceEmployee(JoinPoint joinPoint) {
		rec.log("main","After Advice Method in DeptEmpUI Application Employee controller "+joinPoint.getSignature().getName()
				+ "  Method");
	
	}

	


	  




}
