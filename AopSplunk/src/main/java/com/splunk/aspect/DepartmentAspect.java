package com.splunk.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import com.splunk.Receiver;
import com.splunk.Service;
import com.splunk.config.SplunkConfig;



@Aspect
@Component
public class DepartmentAspect {
	
	
	Service service = SplunkConfig.getSplunkService();
	Receiver rec = service.getReceiver();
	
	@Pointcut("execution(* com.rest.dao.DepartmentDao.*(..))")
	public void departmentRepository() {
		
	}
	

	@Pointcut("execution(* com.rest.service.DepartmentService.*(..))")
	public void departmentService() {
		
	}
	

	@Pointcut("execution(* com.rest.controller.DepartmentController.*(..))")
	public void departmentController() {
		
	}
	
	
	
	@Before("departmentService() && departmentController()")
	public void beforeAdvice(JoinPoint joinPoint) {
		rec.log("main","Before Advice Method in Department Application  "+joinPoint.getSignature().getName()
				+ "  Method");
	

	}

	@After("departmentService() && departmentController()")
	public void afterAdviceMethod(JoinPoint joinPoint) {
		rec.log("main","After Advice Method in Department Application  "+joinPoint.getSignature().getName()
				+ "  Method");
	
	}

	

	

}
