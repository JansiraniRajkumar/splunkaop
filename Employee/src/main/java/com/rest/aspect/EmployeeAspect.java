package com.rest.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.rest.model.Employee;


@Aspect
@Component
public class EmployeeAspect {
	
	Logger logger = LoggerFactory.getLogger(EmployeeAspect.class);  
	

	@Before(value = "execution(* com.rest.service.EmployeeService.insertEmployee(..)) and args(emp)")
	public void beforeAdvice(JoinPoint joinPoint, Employee emp) {
		System.out.println("Before method:" + joinPoint.getSignature());
		System.out.println("Creating Employee with employee Id - " + emp.getEmpId() + ", employee name - "
				+ emp.getEmpName()+ " , Employee age - " +emp.getAge()+ ",  dept id - "+emp.getDeptId());
		logger.info("Before Advice Method in Employee application");		

	}

	@After(value = "execution(* com.rest.service.EmployeeService.insertEmployee(..)) and args(emp)")
	public void afterAdviceMethod(JoinPoint joinPoint,Employee emp) {
		System.out.println("After method:" + joinPoint.getSignature());
		System.out.println("Creating Employee with employee Id - " + emp.getEmpId() + ", employee name - "
				+ emp.getEmpName()+ " , Employee age - " +emp.getAge()+ ",  dept id - "+emp.getDeptId());
		logger.info("After Advice Method in Employee application");			
				
	}

	@AfterReturning(value = "execution(* com.rest.service.EmployeeService.insertEmployee(..)) and args(emp)", returning = "result")
	public void afterReturning(JoinPoint joinPoint,Employee emp, Object result) {
		System.out.println("Method : " + joinPoint.getSignature() + " result : " + result);
		logger.info("After Returning Advice Method in Employee application");	
	}


	@Around(value = "execution(* com.rest.service.EmployeeService.insertEmployee(..)) and args(emp)")
	public void aroundAdvice(ProceedingJoinPoint joinPoint, Employee emp ) throws Throwable {
		System.out.println("Around method: " + joinPoint.getSignature());
		joinPoint.proceed();
		logger.info("Around Advice Method in Employee application");	
		

	}   

	@Pointcut(value = "execution(* com.rest.service.EmployeeService.deleteEmployee(..))")
	private void getEmployeesById() {
	}

	@AfterThrowing(value = "getEmployeesById()", throwing = "exception")
	public void afterThrowingAdvice(JoinPoint jp, Throwable exception) {
		System.out.println("Inside afterThrowingAdvice() method : " + jp.getSignature().getName() + " method");
		System.out.println("Exception= " + exception);
		logger.info("After Throwing Advice Method in Employee application");	
	}   

	
	
	


}
